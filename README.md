# SpammerBlaster

Have you been inundated with spam marketing emails? SpammerBlaster is a simple PHP script that sends a specified number of Unsubscribe requests to the spammer's email.

## Installation

Fork and run locally using WAMP , EasyPHP or upload to your remote server. It uses PHPMailer to send emails, they are included in the project

```git
git clone https://gitlab.com/densman/spamblaster.git
```

## Usage
Add the spammer email address and set number of emails to send
```php
#enter email of your spammer
    $spammer_email = '';
    $no_of_emails_to_send = 5; #change to what ever
```
Update the following section with your SMTP information and reload the page
```php
      $mail->Host = "";  // specify main and backup server
      $mail->SMTPAuth = true;     // turn on SMTP authentication
      $mail->Username = "";  // SMTP username
      $mail->Password = ''; // SMTP password
      $mail->SMTPSecure = 'ssl';
      $mail->Port = 465; #SSL 465
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)